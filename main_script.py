import csv
import json
import requests
import time

# https://dev.tenant-platform-dev.com/v3/applications/app46ee9e2226c4443293a471b42dab6c20/v1/owners/owncb9a086e25a74bc584a6a7a349e59574/facilities/faca0718f554cb85f5f9d2f29a4ec0da4eb/numbers/yelp/

# PUT /{communication-app-id}/v1/owners/{ownerId}/facilities/{facilityId}/numbers/{source}/


def call_links(input, app_id):
    """does PUT requests for every line in the input file

    params:
        input: csv file
        app_id: app id to be used in PUT url
    output:
        prints success or error messag with its data to the terminal
    """
    # replace this to use custom app id
    # app_id = "app46ee9e2226c4443293a471b42dab6c20"

    with open(input, "r") as csv_file:
        # open file, create file reader
        file_reader = csv.reader(csv_file, delimiter=",")

        # get/skip column names from the csv file
        column_names = next(file_reader)

        # get list of allowed sources
        allowed_sources = []
        for row in file_reader:
            if row[6] != "":
                allowed_sources.append(row[6])

        # go to beginning and skip first row(column names)
        csv_file.seek(0)
        next(file_reader)

        # loop through each line in csv file
        # input begins in the file from line 2, line 1 is column names
        line_number = 2
        for row in file_reader:
            print(f"Processing Input file Line:{line_number}")
            # check if source is present and in allowed list
            try:
                source = row[3]
                if source == "":
                    raise ValueError
            except ValueError:
                print(f"MISSING ERROR: Line:{line_number}: Source is missing")
            except Exception as err:
                print(f"UNEXCEPTED ERROR: at Line:{line_number}: {err}")
            else:
                if source not in allowed_sources:
                    print(f"VALUE ERROR: Line:{line_number}: Source({source}) is not in allowed source list.")

            # check if owner is present and of correct format
            try:
                owner = row[4]
                if owner == "":
                    raise ValueError
            except ValueError:
                print(f"MISSING ERROR: Line:{line_number}: Owner is missing")
            except Exception as err:
                print(f"UNEXCEPTED ERROR: at Line:{line_number}: {err}")
            else:
                if not owner.startswith("own"):
                    print(f"VALUE ERROR: Line:{line_number}: Owner({owner}) maybe of wrong format.")

            # check if facility is present and of correct format
            try:
                facility = row[5]
                if facility == "":
                    raise ValueError
            except ValueError:
                print(f"MISSING ERROR: Line:{line_number}: Facility is missing")
            except Exception as err:
                print(f"UNEXCEPTED ERROR: at Line:{line_number}: {err}")
            else:
                if not facility.startswith("fac"):
                    print(f"VALUE ERROR: Line:{line_number}: Facility({facility}) maybe of wrong format.")

            # check if number is present and of correct format, if yes append '+1'
            try:
                number = row[0]
                if number == "":
                    raise ValueError
            except ValueError:
                print(f"MISSING ERROR: Line:{line_number}: Number is missing")
            except Exception as err:
                print(f"UNEXCEPTED ERROR: at Line:{line_number}: {err}")
            else:
                if len(number) != 10:
                    print(f"VALUE ERROR: Line:{line_number}: Number({number}) maybe of wrong format.")
                else:
                    number = f"+1{row[0]}"

            # prepare url and other http data
            put_url = f"https://dev.tenant-platform-dev.com/v3/applications/{app_id}/v1/owners/{owner}/facilities/{facility}/numbers/{source}/"

            custom_data = json.dumps({"number": number, "provider": "call_potential"}) # row[0]

            custom_header = {
                "X-storageapi-key": "309365e7685b4b048d79dc7d29bd4f57",
                "X-storageapi-date": str(int(time.time())),
                "Content-Type": "application/json",
                "X-storageapi-trace-id":"dni-call-potential-script-engg",
            }
            
            # PUT request for every line in the csv input file
            response = requests.put(
                url=put_url, headers=custom_header, data=custom_data
            )
            data = response.json()

            # code to print the result message when PUT is successful or not
            status = data["applicationData"][app_id][0]["status"]
            if status == "error":
                # print error message and data
                result_data = data["applicationData"][app_id][0]["data"]
                print("status: error")
                print("data: ")
                print(json.dumps(result_data, indent=4))
            else:
                # print success message and data
                result_data = data["applicationData"][app_id][0]["data"]
                print("status: success")
                print("data: ")
                print(json.dumps(result_data, indent=4))


            print("-------------\n")
            # increment the line number
            line_number = line_number + 1


def main():
    file = "input_for_testing.csv"
    app_id = "app46ee9e2226c4443293a471b42dab6c20"
    call_links(file, app_id=app_id)


if __name__ == "__main__":
    main()
