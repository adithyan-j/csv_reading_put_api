import csv
import requests
import json
import time

# https://dev.tenant-platform-dev.com/v3/applications/app46ee9e2226c4443293a471b42dab6c20/v1/owners/owncb9a086e25a74bc584a6a7a349e59574/facilities/faca0718f554cb85f5f9d2f29a4ec0da4eb/numbers/yelp/

# print(string)

# with open('input.csv', 'r') as csv_file:
#     # open file create file reader
#     file_reader = csv.reader(csv_file, delimiter=',')

#     next(file_reader)
#     print(next(file_reader)[1])
#     print(type(next(file_reader)[1]))


# put_url = "https://dev.tenant-platform-dev.com/v3/applications/app46ee9e2226c4443293a471b42dab6c20/v1/owners/owncb9a086e25a74bc584a6a7a349e59574/facilities/faca0718f554cb85f5f9d2f29a4ec0da4eb/numbers/yelp/"

app = "app46ee9e2226c4443293a471b42dab6c20"
owner = "owncb9a086e25a74bc584a6a7a349e59574"
facility = "faca0718f554cb85f5f9d2f29a4ec0da4eb"
source = "yelp"

put_url  = f"https://dev.tenant-platform-dev.com/v3/applications/{app}/v1/owners/{owner}/facilities/{facility}/numbers/{source}/"
# print(f"url : {url}")

custom_data = {
    "number": "+15556667779",
    "provider": "call_potential"
}

custom_header = {
    "X-storageapi-key":"309365e7685b4b048d79dc7d29bd4f57",
    "X-storageapi-date":str(int(time.time())),
    "Content-Type":"application/json"
}

print(json.dumps(custom_header, indent=4))

# response = requests.put(url=put_url, data=json.dumps(custom_data), headers=custom_header)

# data = response.json()

# print(json.dumps(data, indent=4))
